/***********************************\
 ***********************************
      A (very) basic buck game
           Mark Peterson
            2014 May 22
   mark.phillip.peterson@gmail.com
 ***********************************
\***********************************/

// Note, most of the base set up is borrowed from
// http://www.smashingmagazine.com/2012/10/19/design-your-own-mobile-game/


// NB: To do items are surrounded by rows of "=", e.g.

	// ======= //
	//  TODO   //
	// ======= //


// Set global variables, if needed



// http://paulirish.com/2011/requestanimationframe-for-smart-animating
// shim layer with setTimeout fallback
// This is a hack to ensure that the animation frame can be grabbed
window.requestAnimFrame = (function(){
  return  window.requestAnimationFrame       || 
          window.webkitRequestAnimationFrame || 
          window.mozRequestAnimationFrame    || 
          window.oRequestAnimationFrame      || 
          window.msRequestAnimationFrame     || 
          function( callback ){
            window.setTimeout(callback, 1000 / 60);
          };
})();


/*************************************
 ** Create a namespace for the game **
 *************************************/
 
 var BUCK = {
	// set inital values
	WIDTH: 320, 
	HEIGHT:  480, 
	scale:  1,
	offset: {top: 0, left: 0}, // the position of the canvas in relation to the screen
	entities: [], // store all cards, touches, etc
	tableColor: "#036", 

	// for tracking progress
	score: {
		aGame: 0,
		bGame: 0,
		aHand: 0,
		bHand: 0
	},

	// for tracking current hand
	currHand: {
		trump: null,
		bidder: null,
		bid: null
	},
	
	// =============================== //
	// TODO: grab team name from login //
	// =============================== //
	team: {
		a: "teamA",
		b: "teamB"
	},
	
	// Set Deck information
	cardWidth: 40,
	cardHeight: 60,
	suits : ["C","D","H","S"],
	cardLev : ["9","10","J","Q","K","A"],
	suitColors : {C: "#0f0",
				  D: "#00f",
				  H: "f00",
				  S: "#000"
	},
	deck : null, // built inside of init
	
	// we'll set the rest of these
	// in the init function
	RATIO:  null,
	currentWidth:  null,
	currentHeight:  null,
	canvas: null,
	ctx:  null,
	ua:  null,
	android: null,
	ios:  null,
	
   /***********************
	** The init function **
	***********************/
	init: function() {
   
		// the proportion of width to height
		BUCK.RATIO = BUCK.WIDTH / BUCK.HEIGHT;
		
		// these will change when the screen is resized
		BUCK.currentWidth = BUCK.WIDTH;
		BUCK.currentHeight = BUCK.HEIGHT;
		
		// this is our canvas element
		BUCK.canvas = document.getElementsByTagName('canvas')[0];
		
		// it's important to set this
		// otherwise the browser will
		// default to 320x200
		BUCK.canvas.width = BUCK.WIDTH;
		BUCK.canvas.height = BUCK.HEIGHT;
		
		// the canvas context allows us to 
		// interact with the canvas api
		BUCK.ctx = BUCK.canvas.getContext('2d');
		
		// we need to sniff out android & ios
		// so we can hide the address bar in
		// our resize function
		BUCK.ua = navigator.userAgent.toLowerCase();
		BUCK.android = BUCK.ua.indexOf('android') > -1 ? true : false;
		BUCK.ios = ( BUCK.ua.indexOf('iphone') > -1 || BUCK.ua.indexOf('ipad') > -1  ) ? true : false;
		
		
		// actually build the deck
		BUCK.deck = BUCK.buildDeck();
		
		// listen for clicks
		window.addEventListener('click', function(e) {
			e.preventDefault();
			BUCK.Input.set(e);
		}, false);

		// listen for touches
		window.addEventListener('touchstart', function(e) {
			e.preventDefault();
			// the event object has an array
			// called touches, we just want
			// the first touch
			BUCK.Input.set(e.touches[0]);
		}, false);
		window.addEventListener('touchmove', function(e) {
			// we're not interested in this
			// but prevent default behaviour
			// so the screen doesn't scroll
			// or zoom
			e.preventDefault();
		}, false);
		window.addEventListener('touchend', function(e) {
			// as above
			e.preventDefault();
		}, false);

		// we're ready to resize
		BUCK.resize();

		BUCK.loop();

	},
	
	
	// Function to resize the screen if screen changes
	resize: function() {
	
		BUCK.currentHeight = window.innerHeight;
		// resize the width in proportion
		// to the new height
		BUCK.currentWidth = BUCK.currentHeight * BUCK.RATIO;

		// this will create some extra space on the
		// page, allowing us to scroll pass
		// the address bar, and thus hide it.
		if (BUCK.android || BUCK.ios) {
			document.body.style.height = (window.innerHeight + 50) + 'px';
		}

		// set the new canvas style width & height
		// note: our canvas is still 320x480 but
		// we're essentially scaling it with CSS
		BUCK.canvas.style.width = BUCK.currentWidth + 'px';
		BUCK.canvas.style.height = BUCK.currentHeight + 'px';

		// the amount by which the css resized canvas
		// is different to the actual (480x320) size.
		BUCK.scale = BUCK.currentWidth / BUCK.WIDTH;
		// position of canvas in relation to
		// the screen
		BUCK.offset.top = BUCK.canvas.offsetTop;
		BUCK.offset.left = BUCK.canvas.offsetLeft;

		// we use a timeout here as some mobile
		// browsers won't scroll if there is not
		// a small delay
		window.setTimeout(function() {
				window.scrollTo(0,1);
		}, 1);
	},
	
	
	// this is where all entities will be moved
	// and checked for collisions etc
	update: function() {
		var i,
			checkCollision = false; // we only need to check for a collision
									// if the user tapped on this game tick
 

		
		// spawn a new instance of Touch
		// if the user has tapped the screen
		if (BUCK.Input.tapped) {
			// add a new touch
			BUCK.entities.push(new BUCK.Touch(BUCK.Input.x, BUCK.Input.y));
			// set tapped back to false
			// to avoid spawning a new touch
			// in the next cycle
			BUCK.Input.tapped = false;
			checkCollision = true;
		}
		
		
		// Test addition of a new card
		if(BUCK.entities.length < 2){
			var tempCard = BUCK.deck[Math.floor(Math.random()*BUCK.deck.length)];
			BUCK.entities.push(new BUCK.Card( 40,150,tempCard)); //"AH"
		}
		

		// cycle through all entities and update as necessary
		for (i = 0; i < BUCK.entities.length; i += 1) {
			BUCK.entities[i].update();

			if (BUCK.entities[i].type === 'card' && checkCollision) {
				hit = BUCK.collides(BUCK.entities[i], 
									{x: BUCK.Input.x, y: BUCK.Input.y, r: 7});
									
				if (hit) {
					// ============================================================ //
					// TODO: move card into play, send info for all to display card //
					// ============================================================ //
					
				}

				BUCK.entities[i].remove = hit;
			}

			// delete from array if remove property
			// flag is set to true
			if (BUCK.entities[i].remove) {
				BUCK.entities.splice(i, 1);
			}
		}

	},
	

	// this is where we draw all the entities
	render: function() {

		var i;


		BUCK.Draw.rect(0, 0, BUCK.WIDTH, BUCK.HEIGHT, BUCK.tableColor); //  '#036');


		// cycle through all entities and render to canvas
		for (i = 0; i < BUCK.entities.length; i += 1) {
			BUCK.entities[i].render();
		}

		// display scores and game info
		BUCK.Draw.text('      ' + BUCK.team.a      + "     " + BUCK.team.b      , 20, 30, 14, '#fff');
		BUCK.Draw.text('Game: ' + BUCK.score.aGame + "     " + BUCK.score.bGame , 20, 50, 14, '#fff');
		BUCK.Draw.text('Hand: ' + BUCK.score.aHand + "     " + BUCK.score.bHand , 20, 70, 14, '#fff');
		
		if(BUCK.currHand.trump){
			BUCK.Draw.text('Trump: ' + BUCK.currHand.trump + "(" + BUCK.currHand.bid + " by " + BUCK.currHand.bidder, 20, 90, 14, '#fff');
		}
		
		// Test display of cards
				
	},
	
	
	// the actual loop
	// requests animation frame
	// then proceeds to update
	// and render
	loop: function() {

		requestAnimFrame( BUCK.loop );

		BUCK.update();
		BUCK.render();
	}


}; // Close the main namespace


// checks if two entties are touching
BUCK.collides = function(card, click) {

	// I believe that this tests for the click falling inside the rectangle of the card
//	if ( (card.x < click.x) && (card.y < click.y) && ( (card.x + card.w) > click.x) && ( (card.y + card.h) > click.y ) ) {
	var farRight = card.x + BUCK.cardWidth;
	var bottomEdge = card.y + BUCK.cardHeight;
	
	if ( card.x < click.x && card.y < click.y && farRight > click.x && bottomEdge > click.y){
		return true;
	} else {
		return false;
	}

};




// abstracts various canvas operations into
// standalone functions
BUCK.Draw = {

	clear: function() {
		BUCK.ctx.clearRect(0, 0, BUCK.WIDTH, BUCK.HEIGHT);
	},


	rect: function(x, y, w, h, col) {
		BUCK.ctx.fillStyle = col;
		BUCK.ctx.fillRect(x, y, w, h);
	},

	circle: function(x, y, r, col) {
		BUCK.ctx.fillStyle = col;
		BUCK.ctx.beginPath();
		BUCK.ctx.arc(x + 5, y + 5, r, 0,  Math.PI * 2, true);
		BUCK.ctx.closePath();
		BUCK.ctx.fill();
	},


	text: function(string, x, y, size, col) {
		BUCK.ctx.font = 'bold '+size+'px Monospace';
		BUCK.ctx.fillStyle = col;
		BUCK.ctx.fillText(string, x, y);
	}
	
};

	
	
BUCK.Input = {

	x: 0,
	y: 0,
	tapped :false,

	set: function(data) {
		this.x = (data.pageX - BUCK.offset.left) / BUCK.scale;
		this.y = (data.pageY - BUCK.offset.top) / BUCK.scale;
		this.tapped = true;

	}

};

BUCK.Touch = function(x, y) {

	this.type = 'touch';	// we'll need this later
	this.x = x;			 // the x coordinate
	this.y = y;			 // the y coordinate
	this.r = 5;			 // the radius
	this.opacity = 1;	   // inital opacity. the dot will fade out
	this.fade = 0.05;	   // amount by which to fade on each game tick
	// this.remove = false;	// flag for removing this entity. BUCK.update
							// will take care of this

	this.update = function() {
		// reduct the opacity accordingly
		this.opacity -= this.fade; 
		// if opacity if 0 or less, flag for removal
		this.remove = (this.opacity < 0) ? true : false;
	};

	this.render = function() {
		BUCK.Draw.circle(this.x, this.y, this.r, 'rgba(255,0,0,'+this.opacity+')');
	};

};


BUCK.buildDeck = function() {

	var deck = new Array(); // to hold cards

	for(var i = 0; i < BUCK.suits.length; i++){
		for(var k = 0; k < BUCK.cardLev.length; k++){
			deck.push(BUCK.cardLev[k] + BUCK.suits[i]);
		}
	}
	
	return (deck);
};	


// Make a card on screen
BUCK.Card = function(x,y,cardVal) {

	this.type = 'card';
 
	this.x = x;
	this.y = y;
	this.suit = cardVal.split("")[1];
	this.cardLev = cardVal.split("")[0];
	this.cardVal = cardVal;

	this.remove = false;


	this.update = function() {
		
		// Currently, nothing to do on each iteration.
		// Removal will happen with playing, after moving into screen

	};

	this.render = function() {

		BUCK.Draw.rect(this.x, this.y, BUCK.cardWidth, BUCK.cardHeight, "#fff");
		BUCK.Draw.text(this.cardVal,
					   this.x + 0.2 * BUCK.cardWidth,
					   this.y + 0.6 * BUCK.cardHeight,
					   20, // font size
					   // "#000"); // for testing
					   BUCK.suitColors[this.suit]);
		
	};

};

	
window.addEventListener('load', BUCK.init, false);
window.addEventListener('resize', BUCK.resize, false);




