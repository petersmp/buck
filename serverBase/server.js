var http = require("http");
var url = require('url');
var fs = require('fs');
var io = require('socket.io');
//var iob = require('socket.io').listen(8002);

var server = http.createServer(function(request, response){
	console.log('Connection');
	var path = url.parse(request.url).pathname;

	switch(path){
		case '/':
			response.writeHead(200, {'Content-Type': 'text/html'});
			response.write('hello world');
			response.end();
			break;
		case '/socket.html':
			fs.readFile(__dirname + path, function(error, data){
				if (error){
					response.writeHead(404);
					response.write( "opps this Page doesn't exist - 404");
					response.end();
				}
				else{
					// console.log('serving ' + path + "\n" + data)
					// this accurately prints both the path and the html to the console
					
					// to actually get this to work, response end needs to be here as
					//  otherwise, the response.end(); below is called before the file is read
					//  or written to response.
					response.writeHead(200, {"Content-Type": "text/html"});
					response.write(data , "utf8");
					console.log('this connected');
					

					response.end();
				}
			});
			break;
		default:
			response.writeHead(404);
			response.write("opps this doesn't exist - 404");
			response.end();
			break;
	}
//	
});

server.listen(8001);

// Needs to lack the "var" that was on the tutorial
io.listen(server);

//iob.listen(server);
// This apparently works when iniitialized above, but not on the same socket?

// Or, needs to be server instead of io.sockets
server.on('connection', function(socket){
	setInterval(function(){
		socket.emit('message', {'message': 'hello world'});
    	console.log('This sent');
    },5000)
});

/*
setInterval(function(){
    console.log('hello world');
}, 1000);
*/
