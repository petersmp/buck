var io = require('socket.io').listen(8000);

io.sockets.on('connection', function (socket) {
	socket.emit('news', { hello: 'world' });
  socket.on('set nickname', function (name) {
    socket.set('nickname', name, function () {
      socket.emit('ready', {data: "ready when  you are"});
    });
  });

  socket.on('msg', function (myNote) {
    socket.get('nickname', function (err, name) {
      console.log('Chat message by ', name, ": ",myNote);
    });
  });
});
