A javascript/HTML5 Buck game
============================

This repo will hold my attempts to build a multiplayer,
web-based game of buck.

All of the actual game information is in the directory
**actualBuck** as I was playing around with server stuff in the other directories.

Goals
-----

- No server side required -- **This appears impossible now**
	- Apparently, P2P connections are not generally allowed due to security issues

- Mobile compatible
	- Instead of building an app, I'd rather just make this work on the web
	- Inspired by 2048 and <http://xkcd.com/1367/>
	- HTML5 canvase should allow it

- Learn JavaScript
	- Need a good excuse, so why not


General Ideas
-------------


- Pass information between clients **now needs server**
- Color code in four colors, instead of 2 *done*
- Change color of the left bauer after bid is settled
- Track score of game and hand (and current trump/bid) on screen *done*
- VERY long term -- create a passable AI player
	- Ideally, one that is tunable to allow examination of effects of strategy


Layout of the game
------------------

- Server
	- Handle player login/ connections
	- Set up deck, shuffle and deal
	- Send each player their hand
	- Accept player bids/card actions (confirm valid)
	- Send bids/card actions
	- Track full game state (may as well archive for later)
	- Eventually, handle timeouts
- Client
	- Handle the variable E/N/W nature of other players
	- Accept their hands
	- Accept other player card actions and display
	- Accept score/hand state and display
	- Send own card actions
	- Send bids (likely click buttons?)
	- Select trump suit

- To move current progress
	- Move game construction out of client (e.g. building the deck)


Progress
--------

- Have a very basic ui built to display cards and scores
- recognizes when a card is clicked
